import fs from 'fs';

import generateDoc from './generate-doc';
import * as sourceData from './source';
import CircularProgress from './circular-progress';
import ProgressBar from './progress-bar';

const INCH = 72;
const BASIC_FONT_SIZE = INCH / 8;
const BASIC_LINE_WIDTH = INCH / 36;
const DEFAULT_FONT = 'Helvetica';
const BOLD_FONT = 'Helvetica-Bold';
const DEFAULT_COLOR = 'white'; // 'black';
const PAGE_WIDTH = 8.5 * INCH;
const PAGE_HEIGHT = 11 * INCH;
const MARGIN = INCH;
const PADDING = BASIC_FONT_SIZE / 3;
const PARAGRAPH_GAP = BASIC_FONT_SIZE;
const $primaryBackground = '#2e3240';

const commonOptions = {
  margin: MARGIN,
};
const colWidth = (PAGE_WIDTH - MARGIN * 2) / 12;

const onPageAdded = ({ logo }) => ({
  event: 'pageAdded',
  method: (doc) => {
    doc
      .rect(
        0,
        0,
        PAGE_WIDTH,
        PAGE_HEIGHT,
      )
      .fill($primaryBackground)
      .image(
        logo,
        INCH / 4,
        INCH / 4,
        { height: BASIC_FONT_SIZE * 2, align: 'center' },
      );
  },
});

const setFont = (doc) => {
  doc
    .font(DEFAULT_FONT)
    .fontSize(BASIC_FONT_SIZE)
    .fillColor(DEFAULT_COLOR);
  return doc;
};

const pageTitle = ({ name }) => (doc) => {
  doc
    .rect(
      0,
      0,
      PAGE_WIDTH,
      PAGE_HEIGHT,
    )
    .fill($primaryBackground);
  doc
    .fillColor(DEFAULT_COLOR)
    .fontSize(BASIC_FONT_SIZE * 4)
    .text('Overall Report', MARGIN, PAGE_HEIGHT * 0.3, { align: 'center' })
    .fontSize(BASIC_FONT_SIZE * 3)
    .text(`Scores, Questionnaries for ${name} `, MARGIN, PAGE_HEIGHT * 0.5, { align: 'center' });
  doc
    .moveTo(MARGIN, PAGE_HEIGHT * 0.7)
    .lineWidth(BASIC_LINE_WIDTH)
    .lineTo(PAGE_WIDTH - MARGIN, PAGE_HEIGHT * 0.7)
    .stroke(DEFAULT_COLOR)
    .fontSize(BASIC_FONT_SIZE)
    .text('CYGOV (c). Visualizing and Simplifying Your Cyber Risk', MARGIN, PAGE_HEIGHT * 0.8, { align: 'center' });
  return doc;
};

const placeChartElement = (doc, element) => {
  const { tag, data } = element;
  if (tag === 'text') {
    const {
      x,
      y,
      color = DEFAULT_COLOR,
      font = BASIC_FONT_SIZE,
      options = {},
    } = element;
    doc
      .fontSize(font)
      .fillColor(color)
      .text(data, x, y, options)
      .font(DEFAULT_FONT)
      .fontSize(BASIC_FONT_SIZE)
      .fillColor(DEFAULT_COLOR);
  }
  if (tag === 'path') {
    const { lineWidth, lineCup, color } = element;
    doc
      .lineCap(lineCup || 'square')
      .lineWidth(lineWidth || BASIC_LINE_WIDTH)
      .path(data)
      .stroke(color);
  }
};

const placeCircularChart = (doc, scores) => {
  const { target, average, total } = scores;
  const radius = INCH * 1.33;
  const circular = new CircularProgress(
    { target, score: total, average },
    {
      x: PAGE_WIDTH / 2,
      y: radius * 2,
      lineWidth: BASIC_LINE_WIDTH * 1.5,
      radius,
      colors: {
        baseText: 'black',
        baseLine: 'gray',
        targetText: '#1bd964',
        'score-low-stroke': '#f75f57', // $carnation
        'score-medium-stroke': '#f7d857', // $energyYellow
        'score-high-stroke': '#1bd964', // $turquoiseBlue
        score: 'yellow',
      },
    },
  );
  const { elements } = circular.getChart();
  elements.map(element => placeChartElement(doc, element));
};

const placeBarChart = (doc, scores) => {
  const { remediation, collection } = scores;
  const width = INCH * 5;
  const x = (PAGE_WIDTH - width) / 2;
  const collectionBar = new ProgressBar(
    collection,
    {
      caption: 'Collection',
      x,
      y: INCH * 6,
      lineWidth: BASIC_LINE_WIDTH * 12,
      padding: BASIC_LINE_WIDTH * 6,
      lineLength: INCH * 5,
      colors: {
        baseText: 'black',
        baseLine: 'gray',
        score: doc.linearGradient(x, 0, x + width / 2, 0)
          .stop(0, '#29A9EB').stop(1, '#0B517C'),
      },
    },
  );
  const remediationBar = new ProgressBar(
    remediation,
    {
      caption: 'Remediation',
      x,
      y: INCH * 8,
      lineWidth: BASIC_LINE_WIDTH * 12,
      padding: BASIC_LINE_WIDTH * 6,
      lineLength: INCH * 5,
      colors: {
        baseText: 'black',
        baseLine: 'gray',
        score: doc.linearGradient(x, 0, x + width / 2, 0)
          .stop(0, '#6A11C3').stop(1, '#340E60'),
      },
    },
  );
  [collectionBar.getChart(), remediationBar.getChart()]
    .map(chart => chart.map(element => placeChartElement(doc, element)));
};

const pageChart = ({ scores }) => (doc) => {
  placeCircularChart(doc, scores);
  placeBarChart(doc, scores);
  return doc;
};

const addPage = (doc) => {
  doc.addPage();
  return doc;
};

const tableHeader = [
  {
    text: 'Security Control',
    offset: 0,
    width: 1,
    options: { align: 'center' },
  },
  {
    text: 'Severity Level',
    offset: 1,
    width: 1,
    options: { align: 'center' },
  },
  {
    text: 'Description',
    offset: 2,
    width: 9,
    options: { align: 'center' },
  },
  {
    text: 'Result',
    offset: 11,
    width: 1,
    options: { align: 'center' },
  },
];

const printQuestion = (doc, values) => {
  let curY = doc.y;
  const sorted = values
    .map(value => ({
      height: doc.heightOfString(
        value.text,
        { ...value.options, width: value.width * colWidth - PADDING * 2 },
      ),
      value,
    }))
    .sort((a, b) => a.height - b.height);
  const heighest = sorted[sorted.length - 1];
  if (curY + heighest.height > PAGE_HEIGHT - MARGIN) {
    doc.addPage();
    printQuestion(doc, tableHeader);
    curY = doc.y;
  }
  sorted.forEach(({ value }) => {
    const {
      text,
      offset,
      width,
      options,
    } = value;
    doc
      .fillColor(DEFAULT_COLOR)
      .text(
        text,
        MARGIN + offset * colWidth + PADDING,
        curY,
        { ...options, width: width * colWidth - PADDING * 2 },
      );
    doc
      .lineWidth(1)
      .rect(
        MARGIN + offset * colWidth + PADDING / 2,
        curY - PARAGRAPH_GAP,
        width * colWidth,
        heighest.height + PARAGRAPH_GAP + 1,
      )
      .stroke(DEFAULT_COLOR);
  });
  doc.moveDown();
};

const printTable = (doc, survey, names = []) => {
  if (survey.question) {
    const { isIgnore, sal, title } = survey.question;
    const { name, score } = survey;
    if (!isIgnore) {
      const line = [
        {
          text: name,
          offset: 0,
          width: 1,
          options: { align: 'center' },
        },
        {
          text: sal,
          offset: 1,
          width: 1,
          options: { align: 'center' },
        },
        {
          text: title,
          offset: 2,
          width: 9,
          options: { align: 'justify' },
        },
        {
          text: score > 0 ? 'YES' : 'NO',
          offset: 11,
          width: 1,
          options: { align: 'center' },
        },
      ];
      printQuestion(doc, line);
    }
  } else {
    const { name, children } = survey;
    if (children) {
      const newList = names.concat(name);
      children.map(child => printTable(doc, child, newList));
    }
  }
};

const printQuestionare = (doc, survey) => {
  doc
    .fontSize(BASIC_FONT_SIZE);
  printQuestion(doc, tableHeader);
  printTable(doc, survey);
};

const printResults = (doc, scores) => {
  const { remediation, collection } = scores;
  const width = INCH * 3;
  let x = MARGIN;
  const { y } = doc;
  const collectionBar = new ProgressBar(
    collection,
    {
      caption: 'Collection',
      x,
      y,
      lineWidth: BASIC_LINE_WIDTH * 6,
      padding: BASIC_LINE_WIDTH * 3,
      lineLength: width,
      colors: {
        baseText: 'black',
        baseLine: 'gray',
        score: doc.linearGradient(x, 0, x + width / 2, 0)
          .stop(0, '#29A9EB').stop(1, '#0B517C'),
      },
    },
  );
  x = PAGE_WIDTH / 2;
  const remediationBar = new ProgressBar(
    remediation,
    {
      caption: 'Remediation',
      x,
      y,
      lineWidth: BASIC_LINE_WIDTH * 6,
      padding: BASIC_LINE_WIDTH * 3,
      lineLength: width,
      colors: {
        baseText: 'black',
        baseLine: 'gray',
        score: doc.linearGradient(x, 0, x + width / 2, 0)
          .stop(0, '#6A11C3').stop(1, '#340E60'),
      },
    },
  );
  [collectionBar.getChart(), remediationBar.getChart()]
    .map(chart => chart.map(element => placeChartElement(doc, element)));
  doc.moveDown(3);
};

const printScores = (doc, scores) => {
  const { target, total } = scores;
  doc.moveDown();
  doc
    .font(DEFAULT_FONT)
    .fontSize(BASIC_FONT_SIZE * 1.5)
    .fillColor(DEFAULT_COLOR)
    .text('Score:', { lineBreak: false, baseline: 'bottom' })
    .fillColor('green')
    .fontSize(BASIC_FONT_SIZE * 3)
    .text(total.toFixed(1), { lineBreak: false, baseline: 'bottom' })
    .fontSize(BASIC_FONT_SIZE * 1.5)
    .text('/10', { lineBreak: false, baseline: 'bottom' })
    .fillColor(DEFAULT_COLOR)
    .text('. Target: ', { lineBreak: false, baseline: 'bottom' })
    .fontSize(BASIC_FONT_SIZE * 3)
    .text(target.toFixed(1), { lineBreak: false, baseline: 'bottom' })
    .fontSize(BASIC_FONT_SIZE * 1.5)
    .text('/10', { baseline: 'bottom' });
};

const pageAgency = ({ name, scores, surveyCore }) => (doc) => {
  doc
    .font(BOLD_FONT)
    .fontSize(BASIC_FONT_SIZE * 2)
    .text(name, { align: 'center' })
    .font(DEFAULT_FONT)
    .moveDown();
  printScores(doc, scores);
  doc.moveDown();
  printResults(doc, scores);
  doc.moveDown();
  printQuestionare(doc, surveyCore);
  return doc;
};

const generateAgencyPDF = (agencyId) => {
  const agency = sourceData.clients.find(({ id }) => id === agencyId);
  if (!agency) {
    throw new Error(`Agency with id ${agencyId} not found`);
  }

  const receivers = [
    fs.createWriteStream(`agency-upperdeck-${agency.name}.pdf`),
  ];
  const options = {
    ...commonOptions,
    info: {
      Title: `Upperdeck ${agency.name}`,
      Author: 'Cygov',
      Subject: '',
      Keywords: '',
    },
  };
  const events = [onPageAdded(agency)];
  const executors = [
    setFont,
    pageTitle(agency),
    addPage,
    pageChart(agency),
    addPage,
    pageAgency(sourceData.it),
    addPage,
    pageAgency(sourceData.headquarter),
  ];

  generateDoc({
    receivers,
    events,
    executors,
  }, options);
};

export default generateAgencyPDF;
