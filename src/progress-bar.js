const describeLine = (x, y, length) => `M${x},${y} L${x + length},${y}`;

export default class ProgressBar {
  constructor(
    value,
    options,
  ) {
    const {
      caption = '',
      x = 0,
      y = 0,
      lineLength = 100,
      lineWidth = 4,
      padding = 2,
      colors = {
        baseText: 'black',
        baseLine: 'gray',
        score: 'yellow',
      },
    } = options;
    this.caption = caption;
    this.colors = { ...colors };
    this.value = value;
    this.x = x;
    this.y = y;
    this.lineWidth = lineWidth;
    this.padding = padding;
    this.height = (lineWidth + padding) * 2; // 16
    this.width = lineLength + padding * 2;
    this.basePath = describeLine(x + padding, y + padding + lineWidth, lineLength);
    this.valuePath = describeLine(x + padding, y + padding + lineWidth, lineLength * value / 100);
  }

  getCaption() {
    return {
      tag: 'text',
      data: `${this.caption}: ${this.value.toFixed(0)}%`,
      x: this.x,
      y: this.y,
      color: this.colors.baseText,
      font: this.lineWidth,
      options: {
        height: this.lineWidth,
      },
    };
  }

  getBaseLineBar() {
    return {
      tag: 'path',
      data: this.basePath,
      color: this.colors.baseLine,
      lineWidth: this.lineWidth,
      lineCup: 'round',
    };
  }

  getValueLineBar() {
    return {
      tag: 'path',
      data: this.valuePath,
      color: this.colors.score,
      lineWidth: this.lineWidth,
      lineCup: 'round',
    };
  }

  getChart() {
    const elements = [
      this.getCaption(),
      this.getBaseLineBar(),
      this.getValueLineBar(),
    ];
    return elements;
  }

  getWidth() {
    return this.width;
  }

  getHeight() {
    return this.height;
  }
}
