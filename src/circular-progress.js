const getScoreClass = (score) => {
  switch (true) {
    case score <= 3.33:
      return 'score-low-stroke';
    case score < 6.66:
      return 'score-medium-stroke';
    case score >= 6.66:
      return 'score-high-stroke';
    default:
      return '';
  }
};

const getLabelAnchor = (angle) => {
  switch (true) {
    case (angle <= 70):
      return 'start';
    case (angle <= 100):
      return 'middle';
    case (angle <= 260):
      return 'end';
    case (angle <= 280):
      return 'middle';
    default:
      return 'start';
  }
};

const polarToCartesian = (centerX, centerY, radius, angleInDegrees) => {
  const angleInRadians = (angleInDegrees * Math.PI) / 180;
  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  };
};

const describeArc = (x, y, radius, startAngle, endAngle) => {
  const start = polarToCartesian(x, y, radius, endAngle);
  const end = polarToCartesian(x, y, radius, startAngle);
  const arcSweep = endAngle - startAngle <= 180 ? '0' : '1';
  const d = [
    'M',
    start.x,
    start.y,
    'A',
    radius,
    radius,
    0,
    arcSweep,
    0,
    end.x,
    end.y,
  ].join(' ');
  return d;
};

const describeLine = (x, y, radius, angle, length) => {
  const startLine = polarToCartesian(x, y, radius, angle);
  const endLine = polarToCartesian(x, y, radius + length, angle);
  return `M${startLine.x},${startLine.y} L${endLine.x},${endLine.y}`;
};

export default class CircularProgress {
  init(params) {
    const {
      x,
      y,
      lineWidth,
      radius,
      colors,
    } = params;
    this.colors = { ...colors };
    this.MIN_SCORE = 0;
    this.MAX_SCORE = 10;

    this.STARTING_ANGLE = 35;
    this.ENDING_ANGLE = 325;

    this.LINE_STROKE = lineWidth || 4; // 4;
    this.BASE_FONT_SIZE = this.LINE_STROKE * 4; // 16;
    this.BG_ARC_STROKE = this.LINE_STROKE * 1.5; // 6;
    this.FG_ARC_STROKE = this.LINE_STROKE * 3; // 12;


    this.ARC_RADIUS = radius || 120;
    this.LINE_LENGTH = this.LINE_STROKE * 8.25;// 25;
    this.SCORE_LABEL_OFFSET = 10;
    this.LINE_LABEL_RADIUS = this.ARC_RADIUS + this.LINE_LENGTH + 20;

    this.SVG_HEIGHT = this.ARC_RADIUS * 2 + this.LINE_LENGTH * 2 + 10; // 310;
    this.SVG_WIDTH = this.SVG_HEIGHT; // 310;
    this.centerX = x || 0 + this.SVG_WIDTH / 2;
    this.centerY = y || 0 + this.SVG_HEIGHT / 2;

    this.SVG_VIEW_BOX = `20 -25 ${this.SVG_WIDTH} ${this.SVG_HEIGHT}`;

    this.AVAILABLE_ANGLE = this.ENDING_ANGLE - this.STARTING_ANGLE;

    this.minScoreAxisX = 0;
    this.minScoreAxisY = 0;
    this.maxScoreAxisX = 0;
    this.maxScoreAxisY = 0;
    this.backgroundArcData = '';
    this.scoreArcData = '';

    this.targetLineData = '';
    this.targetLabelAnchor = '';
    this.targetLabelAxisX = 0;
    this.targetLabelAxisY = 0;

    this.averageLineData = '';
    this.averageLabelAnchor = '';
    this.averageLabelAxisX = 0;
    this.averageLabelAxisY = 0;

    this.scoreStrokeClassColor = '';

    this.alerts = [];
  }

  constructor(
    { score, average, target },
    options = {
      x: 0,
      y: 0,
      lineWidth: 4,
      colors: {
        baseText: 'black',
        baseLine: 'gray',
        targetText: 'green',
        'score-low-stroke': 'red',
        'score-medium-stroke': 'yellow',
        'score-high-stroke': 'green',
        score: 'yellow',
      },
    },
  ) {
    this.init(options);

    this.score = score || this.MIN_SCORE;
    this.average = average || this.MIN_SCORE;
    this.target = target || this.MAX_SCORE;
    if (this.score < this.MIN_SCORE || this.score > this.MAX_SCORE) {
      this.alerts.push('Invalid Score Value');
      this.score = this.MIN_SCORE;
    }
    if (this.average < this.MIN_SCORE || this.average > this.MAX_SCORE) {
      this.alerts.push('Invalid Average Value');
      this.average = this.MIN_SCORE;
    }
    if (this.target < this.MIN_SCORE || this.target > this.MAX_SCORE) {
      this.alerts.push('Invalid Target Value');
      this.target = this.MIN_SCORE;
    }
    this.setBackgroundArc();
    this.setScoreArc();
    this.setTargetLine();
    this.setAverageLine();
  }

  setBackgroundArc() {
    this.backgroundArcData = describeArc(
      this.centerX,
      this.centerY,
      this.ARC_RADIUS,
      this.STARTING_ANGLE,
      this.ENDING_ANGLE,
    );

    const minScoreAxis = polarToCartesian(
      this.centerX,
      this.centerY,
      this.ARC_RADIUS,
      this.STARTING_ANGLE - this.SCORE_LABEL_OFFSET,
    );
    this.minScoreAxisX = minScoreAxis.x;
    this.minScoreAxisY = minScoreAxis.y;

    const maxScoreAxis = polarToCartesian(
      this.centerX,
      this.centerY,
      this.ARC_RADIUS,
      this.ENDING_ANGLE + this.SCORE_LABEL_OFFSET,
    );
    this.maxScoreAxisX = maxScoreAxis.x;
    this.maxScoreAxisY = maxScoreAxis.y;
  }

  calculateAngle(score) {
    const turn = (this.AVAILABLE_ANGLE / this.MAX_SCORE);
    return score * turn + this.STARTING_ANGLE;
  }

  setScoreArc() {
    const relativeScore = this.calculateAngle(this.score);
    this.scoreArcData = describeArc(
      this.centerX,
      this.centerY,
      this.ARC_RADIUS,
      this.STARTING_ANGLE,
      relativeScore,
    );
    this.scoreStrokeClassColor = getScoreClass(this.score);
  }

  setTargetLine() {
    const targetAngle = this.calculateAngle(this.target);
    this.targetLineData = describeLine(
      this.centerX,
      this.centerY,
      this.ARC_RADIUS,
      targetAngle,
      this.LINE_LENGTH,
    );
    const targetLabelAxis = polarToCartesian(
      this.centerX,
      this.centerY,
      this.LINE_LABEL_RADIUS,
      targetAngle,
    );
    this.targetLabelAxisX = targetLabelAxis.x;
    this.targetLabelAxisY = targetLabelAxis.y;
    this.targetLabelAnchor = getLabelAnchor(targetAngle);
  }

  setAverageLine() {
    const avgAngle = this.calculateAngle(this.average);
    this.averageLineData = describeLine(
      this.centerX,
      this.centerY,
      this.ARC_RADIUS,
      avgAngle,
      this.LINE_LENGTH,
    );
    const avgLabelAxis = polarToCartesian(
      this.centerX,
      this.centerY,
      this.LINE_LABEL_RADIUS,
      avgAngle,
    );
    this.averageLabelAxisX = avgLabelAxis.x;
    this.averageLabelAxisY = avgLabelAxis.y;
    this.averageLabelAnchor = getLabelAnchor(avgAngle);
  }


  getMinScore() {
    return {
      tag: 'text',
      x: this.minScoreAxisX,
      y: this.minScoreAxisY,
      data: this.MIN_SCORE,
      color: this.colors.baseText,
      font: this.BASE_FONT_SIZE,
    };
  }

  getMaxScore() {
    return {
      tag: 'text',
      x: this.maxScoreAxisX,
      y: this.maxScoreAxisY,
      data: this.MAX_SCORE,
      color: this.colors.baseText,
      font: this.BASE_FONT_SIZE,
    };
  }

  getTargetLine() {
    return this.target
      ? {
        tag: 'path',
        data: this.targetLineData,
        lineWidth: this.LINE_STROKE,
        color: this.colors.baseLine,
      }
      : null;
  }

  getTargetLabel() {
    if (this.target) {
      return [
        {
          tag: 'text',
          x: this.targetLabelAxisX,
          y: this.targetLabelAxisY,
          anchor: this.targetLabelAnchor,
          data: 'Target:',
          color: this.colors.baseText,
          font: this.BASE_FONT_SIZE,
        },
        {
          tag: 'text',
          x: this.targetLabelAxisX + this.BASE_FONT_SIZE * 4,
          y: this.targetLabelAxisY,
          anchor: this.targetLabelAnchor,
          data: this.target.toFixed(1),
          color: this.colors.targetText,
          font: this.BASE_FONT_SIZE,
        },
      ];
    }
    return [];
  }

  getAverageLine() {
    return this.average
      ? {
        tag: 'path',
        data: this.averageLineData,
        lineWidth: this.LINE_STROKE,
        color: this.colors.baseLine,
      }
      : null;
  }

  getAverageLabel() {
    if (this.average) {
      return [
        {
          tag: 'text',
          x: this.averageLabelAxisX,
          y: this.averageLabelAxisY,
          anchor: this.averageLabelAnchor,
          data: 'Average:',
          color: this.colors.baseText,
          font: this.BASE_FONT_SIZE,
        },
        {
          tag: 'text',
          x: this.averageLabelAxisX + this.BASE_FONT_SIZE * 4,
          y: this.averageLabelAxisY,
          anchor: this.averageLabelAnchor,
          data: this.average.toFixed(1),
          color: this.colors.baseText,
          font: this.BASE_FONT_SIZE,
        },
      ];
    }
    return [];
  }

  getBackgroundArc() {
    return {
      tag: 'path',
      data: this.backgroundArcData,
      lineWidth: this.BG_ARC_STROKE,
      color: this.colors.baseLine,
    };
  }

  getScoreArc() {
    return {
      tag: 'path',
      data: this.scoreArcData,
      lineWidth: this.FG_ARC_STROKE,
      lineCup: 'round',
      color: this.colors[this.scoreStrokeClassColor] || this.colors.score,
    };
  }

  getMiddle() {
    return [
      {
        tag: 'text',
        x: this.centerX - this.BASE_FONT_SIZE * 4,
        y: this.centerY,
        data: `${this.score ? this.score.toFixed(1) : 0}`,
        color: this.colors[this.scoreStrokeClassColor] || this.colors.score,
        font: this.BASE_FONT_SIZE * 4,
        options: { baseline: 'middle' },
      },
      {
        tag: 'text',
        x: this.centerX + this.BASE_FONT_SIZE * 2,
        y: this.centerY,
        data: `/${this.MAX_SCORE}`,
        color: this.colors[this.scoreStrokeClassColor] || this.colors.score,
        font: this.BASE_FONT_SIZE * 1.25,
        options: { baseline: 'top' },
      },
    ];
  }

  getChart() {
    const elements = [
      this.getMinScore(),
      this.getMaxScore(),
      this.getTargetLine(),
      ...this.getTargetLabel(),
      this.getAverageLine(),
      ...this.getAverageLabel(),
      this.getBackgroundArc(),
      this.getScoreArc(),
      ...this.getMiddle(),
    ].filter(element => !!element);
    return { elements, alerts: this.alerts };
  }

  getWidth() {
    return this.SVG_WIDTH;
  }

  getHeight() {
    return this.SVG_HEIGHT;
  }
}
