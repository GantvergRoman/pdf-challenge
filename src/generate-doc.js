import PDFDocument from 'pdfkit';

const start = options => () => new PDFDocument(options);

const setEvents = events => (doc) => {
  events.forEach(({ event, method }) => doc.on(event, () => method(doc)));
  return doc;
};

const setReceivers = streams => (doc) => {
  streams.forEach(stream => doc.pipe(stream));
  return doc;
};

const finish = () => (doc) => {
  doc.end();
};

const pipe = (f, g) => (...args) => g(f(...args));
const executePipe = (...funcs) => funcs.reduce(pipe);

const generateDoc = ({
  receivers,
  events,
  executors,
}, options) => {
  const queue = [
    start(options),
    setReceivers(receivers),
    setEvents(events),
    ...executors,
    finish(),
  ];
  executePipe(...queue)();
};

export default generateDoc;
